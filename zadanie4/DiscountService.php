<?php
namespace RstGroup\Recruitment\ConferenceSystem\Discount;

use RstGroup\Recruitment\ConferenceSystem\Conference\ConferenceRepository;
/* Klasa potrzebna do przetworzenia danych */
use RstGroup\Recruitment\ConferenceSystem\Conference\Conference;

class DiscountService
{
    protected $allDiscounts = [
        'group', 'code'
    ];

    public function calculate($conferenceId, $attendantsCount = null, $price = null, $discountCode = null, $discountsTypes = [], &$is_error = false, &$error_message = null)
    {
        /*
         * Jeżeli $discountsTypes jest pusty, to przyjmujemy tablicę z $allDiscounts
         * w innym wypadku i tak przyjmujemy tablicę.
         * Switch i tak reaguje na konkrety.
         * */
        empty($discountsTypes) ? $discountsToProcess = $this->allDiscounts : $discountsToProcess = $discountsTypes;

//        if (empty($discountsTypes)) {
//            $discountsToProcess = $this->allDiscounts
//        } else {
//            $discountsToProcess = array_intersect($this->allDiscounts, $discountsTypes);
//        }

        $totalDiscount = 0;
        $excludeCodeDiscount = false;

        /* Brakuje tutaj zainicjowania klasy i proponuję zmianę nazwy */
        $conferenceRepository = new ConferenceRepository\;

        foreach ($discountsToProcess as $discount) {
            switch ($discount) {
                case 'group':
                    //$conference = $conferenceRepository->getConference($conferenceId);

                    if ($conferenceRepository->getConference($conferenceId) === null) {
                        throw new \InvalidArgumentException(sprintf("Conference with id %s not exist", $conferenceId));
                    }

                    //$groupDiscount = $conferenceRepository->getGroupDiscount();

                    if (!is_array($conferenceRepository->getGroupDiscount())) {
                        $is_error = true;
                        $error_message = 'Error';
                        return;
                    }

                    $matchingDiscountPercent = 0;

                    foreach ($conferenceRepository->getGroupDiscount() as $minAttendantsCount => $discountPercent) {
                        if ($attendantsCount >= $minAttendantsCount) {
                            $matchingDiscountPercent = $discountPercent;
                        }
                    }

                    $totalDiscount += $price * (float)"0.{$matchingDiscountPercent}";
                    $excludeCodeDiscount = true;
                    break;
                case 'code':
                    if ($excludeCodeDiscount == true) {
                        /* Jeżeli chemy wykluczyć kod rabatowy to powinniśmy w tym momęcie przerwać zamiast puszczać dalej */
                        break;
                        //continue;
                    }

                    /*
                     * Przypisanie zrobił bym inaczej. Myślę że nie ma potrzeby tworzenia osobnej metody do tego.
                     * Do tego, można w waruku użyć od razu odwołania do metody getConference
                    */
                    //$conference = $this->getConferencesRepository()->getConference($conferenceId);

                    if ($conferenceRepository->getConference($conferenceId) === null) {
                        throw new \InvalidArgumentException(sprintf("Conference with id %s not exist", $conferenceId));
                    }

                    /*
                     * Żeby móc w ogóle użyć metod isCodeNotUsed
                     * należało by najpierw użyć klasy Conference
                     *
                     * Dodatkowo, zmienna $conference nie zawiera takich metod jak isCodeNotUsed oraz markCodeAsUsed,
                     * dlatego należy użyć klasy Conference
                     */

                    $conference = new Conference\;

                    if ($conference->isCodeNotUsed($discountCode)) {
                        list($type, $discount) =  $conference->getDiscountForCode($discountCode);

                        if ($type == 'percent') {
                            $totalDiscount += $price * (float)"0.{$discount}";
                        } else if ($type == 'money') {
                            $totalDiscount += $discount;
                        } else {
                            $is_error = true;
                            $error_message = 'Error';
                            return;
                        }

                        $conference->markCodeAsUsed($discountCode);
                    }
                    break;
            }
        }
        return (float)$totalDiscount;
    }
//    protected function getConferencesRepository()
//    {
//        return new ConferenceRepository();
//    }
}