<?php

    function test(Array $array)
    {
        foreach ($array as $number){
            $number ** 2 > 26 ? (yield $number) : null;
        }
    }

    $test = test([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);

//    Output example
//
//    foreach ($test as $a){
//        echo $a;
//    }

