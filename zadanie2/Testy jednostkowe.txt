Testy jednostkowe to jak sama nazwa wskazuje testy, polegające na przetestowaniu każdej części danego oprogramowania (każdej jednostki).
Testy najlepiej pisać w podobnym nazewnictwie tak jak nazwy klas i metod testowanego przez nas oprogramowania.
Jeżeli posiadamy metodę o nazwie create() to najlepiej metodę testującą nazwać createTest().
Podczas samego testu, możemy np. podstawić dane jako argumenty w metodzie i sprawdzić czy to co nam zwróci metoda jest zgodnę z tym czego oczekiwalismy.

Testy jednostkowe są dobre w sytuacjach pisania jak i samej rozbudowy już istniejącego kodu.
Podczas tworzenia pomaga on nam od razu wyłapywać błędy, a w przyszłości podczas rozbudowy oprogramowania pozwala trzmać się ustalonych wewnątrz regół,
dzięki czemu mamy pewność iż zmiany nie spowodują szkód.